package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println(new File(".").getAbsolutePath());
        //System.out.println(new String(Files.readAllBytes(Paths.get("lektura.txt"))));

        //Dzielenie na poszczególne znaki.
        System.out.println(Stream.of(new String(Files.readAllBytes(Paths.get("lektura.txt")))
                .replaceAll("[.,!?:;]", " ")
                .split(" "))
                .map(String::toLowerCase)
                .map(c -> c.split(""))
                .flatMap(Arrays::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Comparator.comparingLong(Map.Entry::getValue))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new))
                .toString());
        //.forEach(System.out::println);
        System.out.println();
        System.out.println();
        //Słowa zawierające literkę 'q'.
        System.out.println(Stream.of(new String(Files.readAllBytes(Paths.get("lektura.txt")))
                .replaceAll("[.,!?:;—]", "")
                .split(" "))
                .map(String::toLowerCase)
                .map(c -> c.split(" "))
                .flatMap(Arrays::stream)
                .distinct()
                .collect(Collectors.groupingBy(c -> c.contains("q"))));
        System.out.println();
        System.out.println();
        //Grupowanie słów pod względem długości.
        System.out.println("Zadanie 3");
        System.out.println(Stream.of(new String(Files.readAllBytes(Paths.get("lektura.txt")))
                .replaceAll("[,.!?:;—]", " ")
                .split(" "))
                .map(String::toLowerCase)
                .map(c -> c.split(" "))
                .flatMap(Arrays::stream)
                .distinct()
                .collect(Collectors.groupingBy(String::length))
                .toString());
        System.out.println();
        System.out.println();
        //Słowa zaczynające się od wielkiej litery && length > 15
        System.out.println("Słowa zaczynające się od wielkiej litery && length > 15");
        System.out.println(Stream.of(new String(Files.readAllBytes(Paths.get("lektura.txt")))
                .replaceAll("[,.!?:;—]", " ")
                .split(" "))
                .map(c -> c.split(" "))
                .flatMap(Arrays::stream)
                .filter(i -> i.length() > 15 && i.charAt(0) == i.toUpperCase().charAt(0))
                .findAny()
                .orElse("Brak takiego słowa"));
        System.out.println();
        System.out.println();
        //Sprawdź czy wszystkie słowa zawierają tylko małe i wielkie litery
        System.out.println(Stream.of(new String(Files.readAllBytes(Paths.get("lektura.txt")))
                .replaceAll("[,.!?:;—]", " ")
                .split(" "))
                .map(c -> c.split(" "))
                .flatMap(Arrays::stream)
                .allMatch(c -> c.matches("[A-Za-z]+")));
    }
}
